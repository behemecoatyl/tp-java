package org.tp;

import java.util.*;

import org.tp.Screen;

public abstract class ExerciceProvider {


    /**
     * Execute an exercice.
     * @param screen A ref to the main screen to interact with the user inside the provided exercice.
     */
    public abstract void run(Screen screen);

    /**
     * Get a list of all the runnable exercice.
     * @return
     */
    public static ArrayList<ExerciceProvider> getAll() {
        ServiceLoader<ExerciceProvider> ldr = ServiceLoader.load(ExerciceProvider.class);
        TreeMap<String, ExerciceProvider> all = new TreeMap<String, ExerciceProvider>(new Comparator<String>() {
            public int compare(String a, String b) {
                String[] chunk_a = a.split(":");
                String[] chunk_b = b.split(":");

                int r = Integer.compare(Integer.parseInt(chunk_a[0]), Integer.parseInt(chunk_b[0]));
                return r == 0 ? Integer.compare(Integer.parseInt(chunk_a[1]), Integer.parseInt(chunk_b[1])) : r;
            }
        });

        for (ExerciceProvider p : ldr) {
            Tp a = p.getClass().getAnnotation(Tp.class);
            all.put(a.tp() + ":" + a.exercice(), p);
        }
        if (all.isEmpty()) {
            throw new Error ("No ExerciceProvider registered.");
        } else {
            ArrayList<ExerciceProvider> exos_list = new ArrayList<ExerciceProvider>(all.size());
            for (Map.Entry<String,ExerciceProvider> entry : all.entrySet()) {
                exos_list.add(entry.getValue());
            }
            return exos_list;
        }
    }
}
