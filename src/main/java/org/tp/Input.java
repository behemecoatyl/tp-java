package org.tp;

import org.tp.exos.Cylindre;
import org.tp.exos.cylindre.Unit;

import java.util.Scanner;

public abstract class Input<T> {

    public final static RangeInput Range = new RangeInput();

    public final static Input<String> String = new Input<String>() {
        @Override
        protected boolean scan (Scanner s) {
            String d = s.next();
            if (d.length() > 0) {
                setData(d);
            }
            return hasData();
        }
    };

    public final static Input<Integer> UInteger = new Input<Integer>() {
        @Override
        protected boolean scan (Scanner s) {
            try {
                int i = s.nextInt();
                if (i >= 0) {
                    setData(i);
                    return true;
                }
                throw new NumberFormatException();
            } catch (NumberFormatException e) {
                return false;
            }
        }
    };

    public final static Input<Integer> Integer = new Input<Integer>() {
        @Override
        protected boolean scan (Scanner s) {
            try {
                setData(s.nextInt());
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }
    };

    public final static Input<Float> Float = new Input<Float>() {
        @Override
        protected boolean scan (Scanner s) {
            try {
                setData(java.lang.Float.parseFloat(s.nextLine().replace(',', '.')));
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }
    };

    public final static Input<Unit> Unit = new Input<Unit>() {
        @Override
        protected boolean scan (Scanner s) {
            String r = s.nextLine().toLowerCase();
            if (Cylindre.ACCEPTED_UNITS.containsKey(r)) {
                setData(Cylindre.ACCEPTED_UNITS.get(r));
            }
            return hasData();
        }
    };


    private T data;
    public Input() {
        data = null;
    }

    /**
     *
     * @return
     */
    public T getData() {
        return data;
    }

    /**
     * Tell if the input object currently has a valid data.
     * @return
     */
    public boolean hasData(){
        return data != null;
    }

    protected void setData(T s) {
        data = s;
    }

    /**
     * Encapsulate one user input. Clear the containing data, and try to get a new one.
     * @param s The scanner used to retrieve input.
     * @return false if the data is invalid, or true.
     */
    public boolean get(Scanner s) {
        data = null;
        scan(s);
        return hasData();
    }

    /**
     * Retrieve one input and validate, if the input is valid, save the value using setData().
     * @param s The scanner used to retrieve input.
     * @return Wether the retrieved value is valid or not.
     */
    protected abstract boolean scan(Scanner s);
}
