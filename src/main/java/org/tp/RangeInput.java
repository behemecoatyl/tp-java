package org.tp;


import java.util.Scanner;

public class RangeInput extends Input<Integer> {
    private int min, max = 0;

    @Override
    protected boolean scan (Scanner s) {
        try {
            int in = java.lang.Integer.parseInt(s.nextLine());
            if (in >= min && in <= max) {
                setData(in);
            }
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public Input<Integer> between(int min, int max) {
        if (max < min) {
            int tmp = min;
            min = max;
            max = tmp;
        }
        this.min = min;
        this.max = max;
        return this;
    }
}
