package org.tp;

import java.util.ArrayList;
import java.util.Scanner;


public class Screen {
    private ArrayList<ExerciceProvider> exos_list;

    public static final int HEADER_HEIGHT = 50;

    public static void main(String[] args) {
        new Screen().run();
    }

    /**
     * Private constructor
     */
    private Screen() {
        exos_list = ExerciceProvider.getAll();
    }

    /**
     * Provide the main loop.
     */
    private void run() {
        int selected;
        do {
            showMenu();
            selected = ask("--> ", Input.Range.between(0, exos_list.size()));

            if (selected > 0) {
                exec(selected);
            }
        } while (selected > 0);
        System.exit(0);
    }


    // region Output logic
    /**
     * Call show(s, true)
     * @param s The String to show.
     */
    public void show(String s) {
        show(s, true);
    }

    /**
     * Show something on the output.
     * @param s The String to show.
     * @param hasLineFeed Should the output include a newline character.
     */
    public void show(String s, boolean hasLineFeed) {
        if (hasLineFeed) {
            System.out.println(s);
        } else {
            System.out.print(s);
        }
    }

    public void show(Showable obj) {
        obj.affiche(this);
    }

    /**
     * Show a line of '='.
     */
    public void showHeader1() {
        showHeader1("");
    }

    /**
     * Show a String filled it with '=' until its size is HEADER_HEIGHT
     * @param header The title of the menu.
     */
    public void showHeader1(String header) {
        showHeader(header, '=');
    }

    /**
     * Show a line of '-'.
     */
    public void showHeader2() {
        showHeader2("");
    }

    /**
     * Show a String filled it with '-' until its size is HEADER_HEIGHT
     * @param header The title of the menu.
     */
    public void showHeader2(String header) {
        showHeader(header, '-');
    }

    private void showHeader(String header, char fill) {
        StringBuilder s = new StringBuilder(HEADER_HEIGHT);
        s.append(header);
        if (s.length() > 0) {
            s.append(" ");
        }
        while (s.length() < s.capacity()) {
            s.append(fill);
        }
        show(s.toString());
    }

    /**
     * Print an empty line.
     */
    public void println() {
        show("");
    }

    private void showMenu() {
        int ctp = 1;

        showHeader1("Menu");
        showHeader2(" TP" + ctp + " : ");
        for (int i = 1; i <= exos_list.size(); i++) {
            Tp a = exos_list.get(i - 1).getClass().getAnnotation(Tp.class);
            if (ctp < a.tp()) {
                showHeader2(" TP" + ++ctp + " : ");
            }
            show(" " + i + " : " + a.title());
        }
        showHeader2();
        show("0   : Exit.");
        showHeader2();
    }
    // endregion

    // region Input logic
    /**
     * Ask something to the user. Encapsulate the input loop.
     * @param prompt The question.
     * @param in The input validator.
     * @param <T> The type returned by the validator.
     * @return The valid answer given by the user.
     */
    public <T> T ask(String prompt, Input<T> in) {
        Scanner s = new Scanner(System.in);
        do {
            show(prompt, false);
            in.get(s);
        } while (!in.hasData());
        return in.getData();
    }
    // endregion

    /**
     * Launch an exercice.
     * @param exo_id The number of the exercice to run.
     */
    private void exec(int exo_id) {
        ExerciceProvider e = exos_list.get(exo_id - 1);
        Tp a = e.getClass().getAnnotation(Tp.class);

        println();
        showHeader1(a.title());
        println();
        show(a.description());
        showHeader2();
        println();
        e.run(this);
        println();
    }
}
