package org.tp;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Tp {
    int tp();
    int exercice();
    String title();
    String description();
}
