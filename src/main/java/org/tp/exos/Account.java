package org.tp.exos;

import org.tp.ExerciceProvider;
import org.tp.Input;
import org.tp.Screen;
import org.tp.Tp;
import org.tp.exos.compte.Compte;

@Tp(tp = 2, exercice = 5, title = "Compte",
    description = "Soit un compte bancaire modélisé de la façon suivante : \n" +
        "+------------------------------+\n" +
        "|           Compte             |\n" +
        "+------------------------------+\n" +
        "|- titulaire : string          |\n" +
        "|- solde : Réel                |\n" +
        "+------------------------------+\n" +
        "|+ Compte (string, réel)       |\n" +
        "|+ setTitulaire(string) : void |\n" +
        "|+ setSolde(réel) : void       |\n" +
        "|+ getTitulaire() : string     |\n" +
        "|+ getSolde() : réel           |\n" +
        "|+ debiter(int) : void         |\n" +
        "|+ crediter(int) : void        |\n" +
        "|+ afficher() : void           |\n" +
        "+------------------------------+\n" +
        "a°) écrire la classe Compte et tester localement les méthodes,\n" +
        "b°) donnez au groupe situé à votre droite la classe Compte (fichier .class) ainsi que les tous les\n" +
        "fichiers de documentation,\n" +
        "c°) implémentez dans un autre fichier une classe CashRegister permettant de tester les méthodes\n" +
        "de la classe reçue du groupe situé à votre gauche.")
public class Account extends ExerciceProvider {
    @Override
    public void run (Screen screen) {
        screen.showHeader2("Test de la Compte.");
        screen.show(new Compte(
            screen.ask("Qui est le titulaire du compte? ", Input.String),
            screen.ask("Quel est le solde? ", Input.UInteger)
        ));
    }
}
