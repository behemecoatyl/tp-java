package org.tp.exos;

import org.tp.ExerciceProvider;
import org.tp.Input;
import org.tp.Screen;
import org.tp.Tp;
import org.tp.exos.tab.MyTab;

@Tp(tp = 3, exercice = 2, title = "Operations sur les tableaux.",
    description = "Ecrire une classe MyTab qui :\n" +
        " - initialise un tableau de n elements (classe Random),\n" +
        " - calcule la moyenne,\n" +
        " - determine la valeur max et la valeur min des valeurs,\n" +
        " - effectue un decalage circulaire a droite,\n" +
        " - tri(float [] v), trie un tableau par ordre croissant,\n" +
        " - incre (int i), incremente toutes les valeurs du tableau de i,\n" +
        " - affiche(), affiche les valeurs du tableau,\n" +
        " - somme(float [] v) qui fournit la somme des valeurs d’un tableau v de taille n fixe,")
public class Array extends ExerciceProvider {

    @Override
    public void run(Screen screen) {
        int size = screen.ask("Indiquez la taille du tableau : ", Input.UInteger);
        MyTab t = new MyTab(size);
        screen.show("Contenu du tableau : ");
        screen.show(t);
        screen.show("moyenne des elements : " + t.moyenne());
        screen.show("element min : " + t.min());
        screen.show("element max : " + t.max());
        screen.show("decalage circulaire a droite : ");
        t.rightShift();
        screen.show(t);
        screen.show("contenu trie : ");
        t.tri();
        screen.show(t);
    }
}
