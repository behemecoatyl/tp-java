package org.tp.exos;

import org.tp.ExerciceProvider;
import org.tp.Input;
import org.tp.Screen;
import org.tp.Tp;
import org.tp.exos.caisse.Caisse;

@Tp(tp = 2, exercice = 6, title = "Caisse",
    description = "Concevoir une classe permettant la gestion d'une caisse enregistreuse :\n" +
        "• montant de la caisse,\n" +
        "• détail de la caisse (somme en espèces, somme en chèques),\n" +
        "• encaisser un chèque,\n" +
        "• encaisser un montant exact,\n" +
        "• encaisser un montant avec rendu de monnaie (indiquer la somme à rendre),\n" +
        "• réinitialiser le contenu de la caisse.\n" +
        "Voici la modélisation de la caisse :" +
        "+------------------------------------+\n" +
        "|           Caisse                   |\n" +
        "+------------------------------------+\n" +
        "|- sommeEspeces : réel               |\n" +
        "|- sommeCheques : réel               |\n" +
        "+------------------------------------+\n" +
        "|+ Caisse()                          |\n" +
        "|+ Caisse (se : réel, sc : réel)     |\n" +
        "|+ setSommeEspeces(e : réel) : void  |\n" +
        "|+ setSommeCheques(c : réel) : void  |\n" +
        "|+ getSommeEspeces() : réel          |\n" +
        "|+ getSommeCheques() : réel          |\n" +
        "|+ montant() : réel (ou void)        |\n" +
        "|+ detail() : void                   |\n" +
        "|+ encaisserCheque (c : réel) : void |\n" +
        "|+ encaisserEspece (e : réel) : void |\n" +
        "|+ encaisserEspeceRendu              |\n" +
        "|   (mtDu:réel, mtDonné:réel) : void |\n" +
        "|+ initialiserCaisse() : void        |\n" +
        "+------------------------------------+\n" +
        "a°) écrire la classe Caisse et tester localement les méthodes,\n" +
        "b°) donnez au groupe situé à votre droite la classe (fichier .class) ainsi que les tous les fichiers\n" +
        "de documentation,\n" +
        "c°) implémentez dans un autre fichier, la classe Principal permettant de tester les méthodes de\n" +
        "la classe reçue du groupe situé à votre gauche.")
public class CashRegister extends ExerciceProvider {
    @Override
    public void run (Screen screen) {
        screen.showHeader2("Test de la Compte.");
        screen.show(new Caisse(
            screen.ask("Combien en especes? ", Input.UInteger),
            screen.ask("Combien en cheques? ", Input.UInteger)
        ));
    }
}
