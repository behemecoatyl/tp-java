package org.tp.exos;

import org.tp.Input;
import org.tp.Screen;
import org.tp.exos.cylindre.OperationCylindre;
import org.tp.ExerciceProvider;
import org.tp.Tp;
import org.tp.exos.cylindre.Unit;

import java.util.HashMap;
import java.util.Map;

@Tp(tp = 1, exercice = 5, title = "Operation cylindre",
    description = "Ecrire une classe OperationCylindre permettant de déterminer pour un" +
        " cylindre de diamètre D et de hauteur H (donnés en centimètres) :\n" +
        " - le rayon de la base,\n" +
        " - le volume du cylindre,\n" +
        " - la surface de la base,\n" +
        " - le périmètre de la base,\n" +
        " - la surface latérale.")
public class Cylindre extends ExerciceProvider {

    public final static Map<String,Unit> ACCEPTED_UNITS = new HashMap<String,Unit>() {{
        put(Unit.METRE.getAbbrev(), Unit.METRE);
        put(Unit.CENTIMETRE.getAbbrev(), Unit.CENTIMETRE);
        put(Unit.MILLIMETRE.getAbbrev(), Unit.MILLIMETRE);
    }};

    /**
     * Construit un objet OperationCylindre de facon interactive. Puis affiche l'ensemble des propriétés du cylindre.
     * @param screen
     */
    public void run(Screen screen) {
        Unit u = screen.ask("Choisissez l'unité des dimensions du cylindre " + ACCEPTED_UNITS.keySet() + ": ", Input.Unit);
        float d = screen.ask("Entrez le diamètre du cylindre, en " + u.getName() + ": ", Input.Float);
        float h = screen.ask("Entrez la hauteur du cylindre, en " + u.getName() + ": " + ": ", Input.Float);
        screen.show(new OperationCylindre(d, h, u));
    }
}
