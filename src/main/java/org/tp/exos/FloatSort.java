package org.tp.exos;

import org.tp.ExerciceProvider;
import org.tp.Input;
import org.tp.Screen;
import org.tp.Tp;

import java.util.ArrayList;

@Tp(tp = 1, exercice = 4, title = "Comparaison de reels",
    description = "Ecrire une classe CompareReel qui compare 3 nombres réels et les affichent dans l'ordre croissant.")
public class FloatSort extends ExerciceProvider {
    /**
     * echange a[i] et a[j] si a[i] > a[j]
     */
    public void swap(ArrayList<Float> a, int i, int j) {
        if (a.get(i) > a.get(j)) {
            Float tmp = a.get(j);
            a.set(j, a.get(i));
            a.set(i, tmp);
        }
    }

    public void run(Screen screen) {
        ArrayList<Float> a = new ArrayList<Float>(3);

        screen.show("Entrez 3 reels: ");
        screen.println();

        // boucle de saisie
        for (int i = 0; i < 3; i++) {
            Float f = screen.ask("--> ", Input.Float);
            a.add(f);
        }

        screen.show("Avant le tri: " + a.get(0) + " - " + a.get(1) + " - " + a.get(2));

        // effectue le tri
        swap(a, 0, 1);
        swap(a, 1, 2);
        swap(a, 0, 1);

        screen.show("Apres le tri: " + a.get(0) + " - " + a.get(1) + " - " + a.get(2));
    }
}
