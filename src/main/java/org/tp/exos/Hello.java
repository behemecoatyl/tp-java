package org.tp.exos;

import org.tp.ExerciceProvider;
import org.tp.Screen;
import org.tp.Tp;

@Tp(tp = 1, exercice = 1, title = "Hello Java",
    description = "Ecrire une classe Bonjour affichant Bonjour à tous.\nRemplacez la méthode print par la méthode println.")
public class Hello extends ExerciceProvider {
    public void run(Screen screen) {
        screen.show("Hello a tous");
    }
}
