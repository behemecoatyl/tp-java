package org.tp.exos;

import org.tp.Input;
import org.tp.Screen;
import org.tp.ExerciceProvider;
import org.tp.Tp;
import org.tp.exos.ecoleinge.EcoleInge;

@Tp(tp = 2, exercice = 4, title = "Ecole ingenieur",
    description = "Concevoir une EcoleInge : \n" +
        "+---------------------------------------+\n" +
        "|           EcoleInge                   |\n" +
        "+---------------------------------------+\n" +
        "|- EtudiantInge : entier                |\n" +
        "|- EtudiantPrepa : entier               |\n" +
        "+---------------------------------------+\n" +
        "|+ EcoleInge()                          |\n" +
        "|+ EcoleInge (entier, entier)           |\n" +
        "|+ setEtudiantInge(entier) : void       |\n" +
        "|+ setEtudiantPrepa(entier) : void      |\n" +
        "|+ getEtudiantInge() : entier           |\n" +
        "|+ getEtudiantPrepa() : entier          |\n" +
        "|+ nombreEtudiants() : réel             |\n" +
        "|+ detail() : void                      |\n" +
        "|+ ajouterEtudiantInge (entier) : void  |\n" +
        "|+ ajouterEtudiantPrepa (entier) : void |\n" +
        "|+ enleverEtudiantInge(entier) : void   |\n" +
        "|+ enleverEtudiantPrepa(entier) : void  |\n" +
        "|+ initialiser() : void                 |\n" +
        "+---------------------------------------+\n" +
        "a°) écrire la classe EcoleInge et tester localement les méthodes,\n" +
        "b°) donnez au groupe situé à votre droite la classe (fichier .class) ainsi que les tous les fichiers\n" +
        "de documentation,\n" +
        "c°) implémentez dans un autre fichier la classe EcoleInge permettant de tester les méthodes de\n" +
        "la classe reçue du groupe situé à votre gauche.")
public class IngeSchool extends ExerciceProvider {
    @Override
    public void run(Screen screen) {
        screen.showHeader2("Test de la class EcoleInge.");
        screen.show(new EcoleInge(
            screen.ask("Combien d'eleves en prepa? ", Input.UInteger),
            screen.ask("Combien d'eleves en inge? ", Input.UInteger)
        ));
    }
}
