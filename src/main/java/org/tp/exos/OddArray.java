package org.tp.exos;

import org.tp.ExerciceProvider;
import org.tp.Input;
import org.tp.Screen;
import org.tp.Tp;

@Tp(tp = 3, exercice = 1, title = "Tableau des carres d'impair",
    description = "Écrire un programme qui crée un tableau comportant les valeurs des carrés des n premiers\n" +
        "nombres impairs.\n" +
        "La valeur de n sera saisie sur la ligne de commande (type String) puis convertit en int par :\n" +
        "int n = Integer.parseInt(args[0]);")
public class OddArray extends ExerciceProvider {

    @Override
    public void run (Screen screen) {
        int n = screen.ask("Entrez un entier positif: ", Input.UInteger);
        StringBuilder s = new StringBuilder("Valeurs des carrés des "+n+" premiers nombres impairs: \n");
        int[] a = new int[n];
        int odd = 1;
        for (int i = 0 ; i < n; i++, odd += 2) {
            int carre = odd * odd;
            a[i] = carre;
            s.append(carre).append(" ");
        }
        screen.show(s.toString().trim());
    }
}
