package org.tp.exos;

import org.tp.ExerciceProvider;
import org.tp.Screen;
import org.tp.Tp;

@Tp(tp = 1, exercice = 7, title = "Jouer avec Java",
    description = "Soit la chaîne de caractères suivante : 'Jouer avec Java'. Réalisez séquentiellement les opérations :" +
    " - longueur de la chaîne (méthode length())," +
    " - conversion en majuscule (méthode toUpperCase())," +
    " - conversion en minuscule (méthode toLowerCase())," +
    " - concaténation avec la chaîne \":o)\"," +
    " - remplacer le caractère \"r\" par le caractère \"z\" (méthode replace(…))," +
    " - comparer avec la chaîne \"Jouer avec Pierre\".")
public class PlayWithJava extends ExerciceProvider {
    @Override
    public void run (Screen screen) {
        String s = "Jouer avec Java"
            , r = "Jouer avec Pierre";

        screen.show("longueur de la chaine: "    + s.length());
        screen.show("chaine en majuscule: "      + s.toUpperCase());
        screen.show("chaine en minuscule: "      + s.toLowerCase());
        screen.show("chaine concaténée: "        + s + ":o)");
        screen.show("chaine avec remplacement: " + s.replace('r', 'z'));
        screen.show(s + " <=> " + r + " : " + s.compareTo(r));
    }
}
