package org.tp.exos;

import org.tp.ExerciceProvider;
import org.tp.Screen;
import org.tp.Tp;

@Tp(tp = 1, exercice = 8, title = "Jouer avec Java",
    description = "Ecrire une classe utilisant quelques éléments de la classe Math.\n" +
        "Voir la documentation de la classe dans la javadoc")
public class PlayWithMath extends ExerciceProvider {
    @Override
    public void run (Screen screen) {
        float a = -3.72f
            , b = 3.48f;

        screen.show("a = " + a + ", b = " + b);
        screen.show("le plus petit est: " + (Math.min(a, b) == a ? 'a' : 'b'));
        screen.show("a\u00B2 = " + Math.pow(a, 2));
        screen.show("a arrondi: " + Math.round(a));
        screen.show("valeur absolue de a: " + Math.abs(a));
        screen.show("la plus petite des valeurs absolues de a et b est: " +
            (Math.min(Math.abs(a), Math.abs(b)) == Math.abs(a) ? 'a' : 'b'));
        screen.show("la plus grande valeur absolue des valeurs arrondies de a et b est: " +
            Math.max(Math.abs(Math.round(a)), Math.abs(Math.round(b))));

        screen.println();

        int c = 90;
        screen.show("Soit un angle c de 90\u00B0");
        screen.show("sinus de c    = " + Math.sin(c));
        screen.show("cosinus de c  = " + Math.cos(c));
        screen.show("tangente de c = " + Math.tan(c));
        screen.show("cos\u00B2(c) + sin\u00B2(c) = " + (Math.pow(Math.cos(c), 2) + Math.pow(Math.sin(c), 2)));
        screen.show("c en Radian = " + Math.toRadians(c));
    }
}
