package org.tp.exos;

import org.tp.ExerciceProvider;
import org.tp.Input;
import org.tp.Screen;
import org.tp.Tp;

import java.util.Arrays;

@Tp(tp = 1, exercice = 6, title = "Racine",
    description = "Ecrire une classe Racine permettant de résoudre une équation du second degré.")
public class Racine extends ExerciceProvider {

    /**
     * Prépare l'équation pour l'affichage.
     *
     * @param coeffs la liste des coefficients
     * @return l'équation formattée
     */
    protected String equation(String pre, int[] coeffs, String[] factors) {
        StringBuilder s = new StringBuilder();

        for (int i = 0; i < 3; i++) {
            int c = coeffs[i];
            String x = factors[i];
            if (c == 0) { // skip if current coeff is 0
                continue;
            }

            if (s.length() == 0) { // format sign
                if (c < 0) {
                    s.append("-");
                }
            } else {
                s.append(c < 0 ? " - " : " + ");
            }
            c = Math.abs(c);
            if (c != 1 || x.equals("")) { // forbid coeff 1 if used as factor of x
                s.append(c);
            }
            s.append(x);
        }

        if (s.length() != 0) {
            s.append(" = 0");
        }

        return s.insert(0, pre).toString().trim();
    }

    /**
     * Prépare la ou les solution(s) pour l'affichage.
     *
     * @param sol la liste des solutions
     * @return les solutions formattées
     */
    protected String solution(float[] sol) {
        StringBuilder s = new StringBuilder();
        Arrays.sort(sol);

        for (float x : sol) {
            if (s.length() > 0) {
                s.append(", ");
            }
            s.append((int)x == x ? (int)x : x); // required to statically resolve method signature
        }
        return s.insert(0, "S={").append("}").toString();
    }



    /**
     * Résout une equation de la forme ax² + bx + c = 0.
     * @param a
     * @param b
     * @param c
     * @return un tableau de 0, 1 ou 2 valeurs représentant les solutions admisent par l'équation
     */
    public float[] solve(int a, int b, int c) {
        if (a == 0) {
            if (b != 0) { // alors equation du 1er degré -> 1 solution
                return new float[] {-c / (float)b};
            }
        } else { // alors equation du 2nd degré -> 0, 1 ou 2 solutions selon la valeur de delta
            int delta = b*b - 4*a*c;
            float denom = 2f * a;
            if (delta == 0) { // delta = 0 -> 1 double solution
                return new float[] {-b / denom};
            } else {
                if (delta > 0) { // delta superieur a zero -> 2 solutions
                    float sqrd = (float)Math.sqrt(delta);
                    return new float[] {(-b + sqrd) / denom, (-b - sqrd) / denom};
                }
            }
        }
        return new float[0]; // equation sans solution si a == b == 0 ou delta < 0
    }

    @Override
    public void run (Screen screen) {
        screen.show("Calcul d'une équation de la forme ax\u00B2 + bx + c = 0.");
        screen.showHeader2();

        int a, b, c;
        a = screen.ask(" a : ", Input.Integer);
        b = screen.ask(" b : ", Input.Integer);
        c = screen.ask(" c : ", Input.Integer);

        float[] sol = solve(a, b, c);

        screen.show(equation("L'équation ", new int[]{a, b, c}, new String[]{"x\u00B2", "x", ""}), false);
        if (sol.length == 0) {
            screen.show(" n'a pas de solution.");
        } else {
            screen.show((sol.length == 1 ? " admet une solution: " : " admet deux solutions: ") + solution(sol));
        }
    }
}
