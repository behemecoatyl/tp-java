package org.tp.exos;

import org.tp.ExerciceProvider;
import org.tp.Screen;
import org.tp.Tp;
import org.tp.exos.nombre.Nombre;

@Tp(tp = 2, exercice = 2, title = "Nombre aleatoire",
    description = "Ecrire une classe Nombre ayant pour attribut un nombre qui sera initialisé aléatoirement par le\n" +
        "constructeur de la classe.\n" +
        "Une méthode affiche() permet d’afficher ce nombre.\n" +
        "Une autre fichier, contenant la classe School et la méthode main() permet de tester la classe\n" +
        "Nombre. Cette classe sera chargée d’instancier un objet Nombre et d’appeler la méthode\n" +
        "affiche().")
public class RandomNumber extends ExerciceProvider {
    @Override
    public void run(Screen screen) {
        screen.show(new Nombre());
    }
}
