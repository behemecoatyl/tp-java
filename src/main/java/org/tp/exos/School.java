package org.tp.exos;

import org.tp.ExerciceProvider;
import org.tp.Input;
import org.tp.Screen;
import org.tp.Tp;
import org.tp.exos.ecole.Ecole;

@Tp(tp = 2, exercice = 3, title = "Ecole",
    description = "Soit une école modélisée de la façon suivante :\n" +
    "+-----------------------------------+\n" +
    "|               Ecole               |\n" +
    "+-----------------------------------+\n" +
    "|- nom : string                     |\n" +
    "|- nombreEtudiant : entier          |\n" +
    "+-----------------------------------+\n" +
    "|+ Ecole (string, entier)           |\n" +
    "|+ setNom(string) : void            |\n" +
    "|+ setNombreEtudiant(entier) : void |\n" +
    "|+ getNombreEtudiant() : string     |\n" +
    "|+ getNom() : entier                |\n" +
    "|+ enleverEtudiant(entier) : void   |\n" +
    "|+ crediterEtudiant(entier) : void  |\n" +
    "|+ afficher() : void                |\n" +
    "+-----------------------------------+\n" +
    "a°) écrire la classe Ecole et tester localement les méthodes,\n" +
    "b°) donnez au groupe situé à votre droite la classe (fichier .class) ainsi que les tous les fichiers\n" +
    "de documentation,\n" +
    "c°) implémentez dans un autre fichier la classe School permettant de tester les méthodes de\n" +
    "la classe reçue du groupe situé à votre gauche.")
public class School extends ExerciceProvider {
    @Override
    public void run(Screen screen) {
        screen.showHeader2("Test de la class Ecole.");
        screen.show(new Ecole(
            screen.ask("Quel est le nom de l'ecole? ", Input.String),
            screen.ask("Combien a-t-elle d'eleves? ", Input.UInteger)
        ));
    }
}
