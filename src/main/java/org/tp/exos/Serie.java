package org.tp.exos;

import org.tp.ExerciceProvider;
import org.tp.Tp;
import org.tp.Screen;

@Tp(tp = 1, exercice = 3, title = "Serie",
    description = "Ecrire une classe Serie calculant la somme des 6 premiers termes de la série:\n 1 + 1/2 + ... + 1/6")
public class Serie extends ExerciceProvider {

    /**
     * Affiche la somme des 1/i pour i in [1..6].
     */
    @Override
    public void run (Screen screen) {
        float acc = 1f;
        for (int i = 2; i <= 6; i++){
            acc += 1f / i;
        }
        screen.show(String.format("la somme des 1/i pour i de 1 a 6 est %.2f", acc));
    }
}
