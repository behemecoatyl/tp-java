package org.tp.exos;

import org.tp.ExerciceProvider;
import org.tp.Tp;
import org.tp.Screen;

@Tp(tp = 1, exercice = 2, title = "Affiche les nombres",
    description = "Ecrire une classe qui affiche les nombres de 1 à 20 sauf 10 et 12.")
public class ShowNumbers extends ExerciceProvider {

    /**
     * Affiche les nombres de 1 à 20 dans la console, sauf 10 et 12.
     */
    public void run(Screen screen) {
        StringBuilder s = new StringBuilder();
        for(int i = 1; i <= 20; i++) {
            if (i != 10 && i != 12) {
                s.append(" ").append(i);
            }
        }
        screen.show(s.toString().trim());
    }
}
