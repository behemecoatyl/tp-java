package org.tp.exos;

import org.tp.ExerciceProvider;
import org.tp.Input;
import org.tp.Screen;
import org.tp.Tp;

@Tp(tp = 2, exercice = 1, title = "Etoile",
    description = "Ecrire une classe Etoile qui utilise une méthode affiche() permettant de visualiser :\n" +
        "*\n" +
        "* *\n" +
        "* * *\n" +
        "* * * *\n" +
        "La hauteur du triangle sera fournie en donnée.")
public class Star extends ExerciceProvider {
    @Override
    public void run (Screen screen) {
        int h = screen.ask("Quel est la hauteur du triangle? ", Input.UInteger);
        for (int i = 1; i <= h; i++) {
            String s = "";
            for (int j = 1; j <= i; j++) {
                s += "* ";
            }
            screen.show(s.trim());
        }
    }
}
