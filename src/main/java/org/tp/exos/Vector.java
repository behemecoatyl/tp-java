package org.tp.exos;

import org.tp.ExerciceProvider;
import org.tp.Input;
import org.tp.Screen;
import org.tp.Tp;
import org.tp.exos.tab.Vecteur;

@Tp(tp = 3, exercice = 3, title = "Operations sur des vecteurs.",
    description = "Réaliser une classe Vecteur permettant de manipuler des vecteurs ayant un nombre\n" +
        "quelconque de composantes de type double avec :\n" +
        " - un constructeur Vecteur(int n), n représentant le nombre de composantes initialisées à 0\n" +
        " - un constructeur Vecteur(int n,double x), n représentant le nombre de composantes qui seront toutes initialisées à la valeur de x\n" +
        " - un constructeur Vecteur(Vecteur  v) qui créera un vecteur par recopie du tableau v\n" +
        " - une méthode prod fournissant le produit scalaire de 2 vecteurs (si la taille est différente, le produit scalaire vaut 0)\n" +
        " - une méthode somme fournissant la somme de 2 vecteurs; s’ils n’ont pas la même taille, renvoyer la référence «nulle»\n" +
        " - une méthode affiche affichant les composants du vecteur. ")
public class Vector extends ExerciceProvider {

    private Vecteur getVecteur(Screen screen, String prompt, int s) {
        Vecteur v = new Vecteur(s);
        for (int i = 0; i < s; i++) {
            v.set(i, screen.ask("vec" + prompt + "(" + i + ") : ", Input.Float));
        }
        return v;
    }

    @Override
    public void run(Screen screen) {
        int size = screen.ask("Indiquez la taille des vecteurs : ", Input.UInteger);
        Vecteur vec1 = getVecteur(screen, "1", size);
        Vecteur vec2 = getVecteur(screen, "2", size);
        screen.show("vec1 : " + vec1.detail());
        screen.show("vec2 : " + vec2.detail());
        screen.show("produit scalaire de vec1 et vec2 : " + vec1.prod(vec2));
        screen.show("somme des deux vecteurs : " + vec1.somme(vec2).detail());
    }
}
