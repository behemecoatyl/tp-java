package org.tp.exos.caisse;

import org.tp.Screen;
import org.tp.Showable;

public class Caisse implements Showable {
    private float sommeEspeces;
    private float sommeCheques;

    public Caisse() {
        initialiserCaisse();
    }

    /**
     * @param se La somme en espèce présente dans la caisse.
     * @param sc La somme en chèque présente dans la caisse.
     */
    public Caisse(float se, float sc) {
        setSommeCheques(sc);
        setSommeEspeces(se);
    }

    /**
     * @return La somme en espèce présente dans la caisse.
     */
    public float getSommeEspeces() {
        return sommeEspeces;
    }

    /**
     * @param se La somme en espèce présente dans la caisse.
     */
    public void setSommeEspeces(float se) {
        if (se >= 0) {
            sommeEspeces = se;
        }
    }

    /**
     * @return La somme en chèque présente dans la caisse.
     */
    public float getSommeCheques() {
        return sommeCheques;
    }

    /**
     * @param sc La somme en chèque présente dans la caisse.
     */
    public void setSommeCheques(float sc) {
        if (sc >= 0) {
            sommeCheques = sc;
        }
    }

    /**
     * @return La somme totale présente dans la caisse.
     */
    public float montant() {
        return getSommeCheques() + getSommeEspeces();
    }

    /**
     * Affiche l'état de la caisse.
     */
    public String detail() {
        return "Montant en cheque : " + getSommeCheques() +
             "\nMontant en espece : " + getSommeEspeces() +
             "\nMontant total     : " + montant();
    }

    /**
     * Encaisse un montant en espèce.
     *
     * @param e Le montant en espèce a encaisser.
     */
    public void encaisserEspece(float e) {
        setSommeEspeces(getSommeEspeces() + e);
    }

    /**
     * Encaisse un chèque.
     *
     * @param c Le montant du chèque encaisser.
     */
    public void encaisserCheque(float c) {
        setSommeCheques(getSommeCheques() + c);
    }

    /**
     * Encaisse un montant en espèce.
     *
     * @param mtDu La monnaie rendue.
     * @param mtDonne Le montant encaissé.
     */
    public void  encaisserEspeceRendu (float mtDu, float mtDonne) {
        encaisserEspece(mtDonne - mtDu);
    }

    /**
     * Initialise la caisse avec un montant par défaut.
     */
    public void initialiserCaisse() {
        setSommeEspeces(0);
        setSommeCheques(0);
    }

    @Override
    public void affiche (Screen s) {
        s.show(detail());
    }
}
