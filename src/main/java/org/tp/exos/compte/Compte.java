package org.tp.exos.compte;

import org.tp.Screen;
import org.tp.Showable;

public class Compte implements Showable {
    private String titulaire;
    private float solde;

    /**
     * @param n Le nom du titulaire du compte.
     * @param s Le solde du compte.
     */
    public Compte(String n, float s) {
        setSolde(s);
        setTitulaire(n);
    }

    /**
     * @return Le nom du titulaire du compte.
     */
    public String getTitulaire() {
        return titulaire;
    }

    /**
     * @param n Le nom du titulaire du compte.
     */
    public void setTitulaire(String n) {
        titulaire = n;
    }

    /**
     * @return Le solde du compte.
     */
    public float getSolde() {
        return solde;
    }

    /**
     * @param s Le solde du compte.
     */
    public void setSolde(float s) {
        solde = s;
    }

    /**
     * Débite le compte.
     * @param n Le montant à débiter.
     */
    public void debiter(int n) {
        setSolde(getSolde() - n);
    }

    /**
     * Crédite le compte.
     * @param n Le montant à créditer.
     */
    public void crediter(int n) {
        setSolde(getSolde() + n);
    }

    /**
     * Affiche l'état du compte.
     */
    public String detail() {
        return "Titulaire du compte : " + getTitulaire() + "\nSolde du compte     : " + getSolde();
    }

    @Override
    public void affiche (Screen s) {
        s.show(detail());
    }
}
