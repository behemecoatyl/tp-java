package org.tp.exos.cylindre;

import org.tp.Screen;
import org.tp.Showable;
import org.tp.exos.Cylindre;

public class OperationCylindre implements Showable {


    // region attributs
    private float diametre;
    private float hauteur;
    private Unit unit;
    // endregion attributs

    // region constructeurs
    /**
     * constructeur par default
     */
    public OperationCylindre() {
        setDiametre(0f);
        setHauteur(0f);
        setUnit(Unit.CENTIMETRE);
    }

    /**
     * @param d le diametre de la base du cylindre
     * @param h la hauteur du cylindre
     * @param u unite du diametre et de la hauteur
     */
    public OperationCylindre(float d, float h, Unit u) {
        this();
        setDiametre(d);
        setHauteur(h);
        setUnit(u);
    }
    // endregion constructeurs

    // region accesseurs
    /**
     * @return L'unité de mesure utilisée par le cylindre.
     */
    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit u) {
        if (Cylindre.ACCEPTED_UNITS.containsValue(u)) {
            unit = u;
        }
    }

    /**
     * @return le diametre de la base du cylindre
     */
    public float getDiametre() {
        return diametre;
    }

    public void setDiametre(float d) {
        diametre = Math.abs(d);
    }

    /**
     * @return la hauteur du cylindre
     */
    public float getHauteur() {
        return hauteur;
    }

    public void setHauteur(float d) {
        hauteur = Math.abs(d);
    }

    /**
     * @return le rayon de la base du cylindre
     */
    public float getRayon() {
        return getDiametre() / 2;
    }

    public void setRayon(float r) {
        setDiametre(r * 2);
    }


    /**
     * @return le volume du cylindre
     */
    public float getVolume() {
        return getAireBase() * getHauteur();
    }

    /**
     * @return la surface de la base du cylindre
     */
    public float getAireBase() {
        return (float)Math.PI * getRayon() * getRayon();
    }

    /**
     * @return le perimetre de la base du cylindre
     */
    public float getPerimetreBase() {
        return (float)Math.PI * getDiametre();
    }

    /**
     * @return la surface lateral du cylindre
     */
    public float getAireLateral() {
        return getHauteur() * getPerimetreBase();
    }

    /**
     * @return la surface total du cylindre
     */
    public float getAireTotal() {
        return getAireBase() * 2 + getAireLateral();
    }

    @Override
    public void affiche (Screen s) {
        String unit = getUnit().getAbbrev();
        s.println();
        s.show("Le cylindre a les proprietes suivantes:\n");
        s.show(String.format("diametre:             %.2f %s", getDiametre(), unit));
        s.show(String.format("rayon:                %.2f %s", getRayon(), unit));
        s.show(String.format("hauteur:              %.2f %s", getHauteur(), unit));
        s.show(String.format("perimetre de la base: %.2f %s", getPerimetreBase(), unit));
        s.println();
        s.show(String.format("surface de la base:   %.2f %s\u00B2", getAireBase(), unit));
        s.show(String.format("surface lateral:      %.2f %s\u00B2", getAireLateral(), unit));
        s.show(String.format("surface total:        %.2f %s\u00B2", getAireTotal(), unit));
        s.println();
        s.show(String.format("volume:               %.2f %s\u00B3", getVolume(), unit));
    }
    // endregion accesseurs
}
