package org.tp.exos.cylindre;

public enum Unit {
    METRE("m", "metre"),
    CENTIMETRE("cm", "centimetre"),
    MILLIMETRE("mm", "millimetre");

    private final String abbrev;
    private final String name;
    Unit(String a, String n) {
        this.abbrev = a;
        this.name = n;
    }

    public String getName() {
        return name;
    }

    public String getAbbrev() {
        return abbrev;
    }

    public String toString() {
        return getAbbrev();
    }

}
