package org.tp.exos.ecole;

import org.tp.Screen;
import org.tp.Showable;

public class Ecole implements Showable {
    private String nom;
    private int nombreEtudiant;

    /**
     * Créer une école, en spécifiant son nom et le nombre d'élèves.
     * @param nom Le nom de l'école.
     * @param eleves Le nombre d'élèves.
     */
    public Ecole(String nom, int eleves) {
        setNom(nom);
        setNombreEtudiant(eleves);
    }

    /**
     * @return Le nom de l'école.
     */
    public String getNom() {
        return nom;
    }

    /**
     * Modifie le nom de l'école.
     * @param nom Le nom de l'école.
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return Le nombre d'élèves.
     */
    public int getNombreEtudiant() {
        return nombreEtudiant;
    }

    /**
     * Modifie le nombre d'élèves.
     * @param nb Le nombre d'élèves.
     */
    public void setNombreEtudiant(int nb) {
        nombreEtudiant = (nb < 0) ? 0 : nb;
    }

    /**
     * Retranche nb étudiants au compte des élèves.
     * @param nb Le nombre d'étudiants à enlever.
     */
    public void enleverEtudiant(int nb) {
        setNombreEtudiant(getNombreEtudiant() - nb);
    }

    /**
     * Ajoute nb étudiants au compte des élèves.
     * @param nb Le nombre d'étudiants à ajouter.
     */
    public void crediterEtudiant(int nb) {
        setNombreEtudiant(getNombreEtudiant() + nb);
    }

    public String detail() {
        return "Ecole: " + getNom() + " - " + getNombreEtudiant() + " etudiants.";
    }

    /**
     * Affiche le détail de l'école.
     * @param s
     */
    @Override
    public void affiche(Screen s) {
        s.show(detail());
    }
}
