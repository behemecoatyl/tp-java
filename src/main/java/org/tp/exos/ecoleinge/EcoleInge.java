package org.tp.exos.ecoleinge;

import org.tp.Screen;
import org.tp.Showable;

public class EcoleInge implements Showable {
    private int etudiant_inge;
    private int etudiant_prepa;

    /**
     * Créer une école sans élève.
     */
    public EcoleInge() {
        initialiser();
    }

    /**
     * Créer une école en spécifiant le nombre d'élèves.
     * @param prepa Nombre d'élèves en classe préparatoire.
     * @param inge Nombre délèves en classe d'ingénieur.
     */
    public EcoleInge(int prepa, int inge) {
        setEtudiantInge(inge);
        setEtudiantPrepa(prepa);
    }

    /**
     * Initialise l'école avec aucun étudiants.
     */
    public void initialiser() {
        setEtudiantInge(0);
        setEtudiantPrepa(0);
    }

    /**
     * @return Le nombre d'élèves en classe d'ingénieur.
     */
    public int getEtudiantInge() {
        return etudiant_inge;
    }

    /**
     * Indique le nombre d'élèves en classe d'ingénieur.
     * @param nb Le nombre d'élève.
     */
    public void setEtudiantInge(int nb) {
        etudiant_inge = nb < 0 ? 0 : nb;
    }

    /**
     * @return Le nombre d'élèves en classe préparatoire.
     */
    public int getEtudiantPrepa() {
        return etudiant_prepa;
    }

    /**
     * Indique le nombre d'élèves en classe préparatoire.
     * @param nb le nombre d'élève
     */
    public void setEtudiantPrepa(int nb) {
        etudiant_prepa = nb < 0 ? 0 : nb;
    }

    /**
     * @return Le nombre total d'élèves.
     */
    public int nombreEtudiants() {
        return getEtudiantInge() + getEtudiantPrepa();
    }

    /**
     * Ajoute nb étudiants au compte des élèves en classe d'ingénieur.
     * @param nb Le nombre d'étudiants à ajouter.
     */
    public void ajouterEtudiantInge(int nb) {
        setEtudiantInge(getEtudiantInge() + nb);
    }

    /**
     * Retranche nb étudiants au compte des élèves en classe d'ingénieur.
     * @param nb Le nombre d'étudiants à enlever.
     */
    public void enleverEtudiantInge(int nb) {
        setEtudiantInge(getEtudiantInge() - nb);
    }

    /**
     * Ajoute nb étudiants au compte des élèves en classe préparatoire.
     * @param nb Le nombre d'étudiants à ajouter.
     */
    public void ajouterEtudiantPrepa (int nb) {
        setEtudiantPrepa(getEtudiantPrepa() + nb);
    }

    /**
     * Retranche nb étudiants au compte des élèves en classe préparatoire.
     * @param nb Le nombre d'étudiants à enlever.
     */
    public void enleverEtudiantPrepa(int nb) {
        setEtudiantPrepa(getEtudiantPrepa() - nb);
    }

    /**
     * Affiche le détail des élèves.
     */
    public String detail() {
        return getEtudiantPrepa()+ " élève(s) en prépa.\n" +
               getEtudiantInge() + " élève(s) en cursus ingénieur.\n" +
               nombreEtudiants() + " étudiant(s).";
    }

    @Override
    public void affiche (Screen s) {
        s.show(detail());
    }
}
