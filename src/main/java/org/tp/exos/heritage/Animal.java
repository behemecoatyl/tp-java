package org.tp.exos.heritage;

import org.tp.Screen;
import org.tp.Showable;

public abstract class Animal implements Showable {
    public static final String[] sexes = new String[] {"male", "femelle"};
    public static String[] types;

    private String nom;
    private String race;
    private String sexe;
    private String type;
    private int age;

    public Animal(String nom, String race, int age) {
        setAge(age);
        setNom(nom);
        setRace(race);
    }

    public String getNom () {
        return nom;
    }

    public void setNom (String nom) {
        this.nom = nom;
    }

    public String getRace () {
        return race;
    }

    public void setRace (String race) {
        this.race = race;
    }

    public int getAge () {
        return age;
    }

    public void setAge (int age) {
        this.age = age;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String s) {
        for (String sexe : sexes) {
            if (s.equals(sexe)) {
                this.sexe = s;
            }
        }
    }

    //public abstract String getType();
    //public abstract void setType(String t);

   // @Override
    public String getType() {
        return type;
    }

   // @Override
    public void setType(String t) {
        for (String type : types) {
            if (t.equals(type)) {
                this.type = t;
            }
        }
    }

    protected String detail() {
        return "Je m’appelle "+getNom()+
            ", je suis un "+getRace()+
            " et j’ai "+getAge()+
            " ans, je suis " + (sexe.equals("femmelle") ? "une " : "un ") + getSexe();
    }

    @Override
    public void affiche (Screen s) {
        s.show(detail());
    }
}
