package org.tp.exos.heritage;

public class Cheval extends Animal {
    public static final String[] types = new String[] {"course", "trot"};

    public Cheval(String nom, int age, String sexe, String type) {
        super(nom, "cheval", age);
        setSexe(sexe);
        setType(type);
    }
}
