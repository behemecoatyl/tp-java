package org.tp.exos.heritage;

public class Chien extends Animal {
    public static final String[] types = new String[] {"appartement", "extérieur"};

    public Chien(String nom, int age, String sexe, String type) {
        super(nom, "chien", age);
        setSexe(sexe);
        setType(type);
    }
}
