package org.tp.exos.heritage;

public class Oiseau extends Animal {
    public static final String[] types = new String[] {"chasseur", "pêcheur"};

    public Oiseau(String nom, int age, String sexe, String type) {
        super(nom, "chien", age);
        setSexe(sexe);
        setType(type);
    }
}
