package org.tp.exos.heritage;

import org.tp.Screen;
import org.tp.Showable;

public class Personne implements Showable {
    private String nom;
    private String prenom;
    private int age;

    public Personne(String n, String p, int a) {
        setAge(a);
        setNom(n);
        setPrenom(p);
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String detail() {
        return getPrenom() + " " + getNom() + " (" + getAge() + " ans)";
    }

    @Override
    public void affiche(Screen s) {
        s.show(detail());
    }
}
