package org.tp.exos.heritage;

import org.tp.Screen;

public class Skieur extends Personne {
    private boolean forfait;

    public Skieur(String n, String p, int a, boolean f) {
        super(n, p, a);
        setForfait(f);
    }

    public boolean hasForfait () {
        return forfait;
    }

    public void setForfait (boolean forfait) {
        this.forfait = forfait;
    }

    @Override
    public void affiche(Screen s) {
        s.show(detail() + (hasForfait() ? "a un forfait." : "n'a pas de forfait."));
    }
}
