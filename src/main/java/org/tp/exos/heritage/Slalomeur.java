package org.tp.exos.heritage;

import org.tp.Screen;

public class Slalomeur extends Skieur {
    private float temp;

    public Slalomeur(String n, String p, int a, boolean f) {
        super(n, p, a, f);
        setTemp(0);
    }

    public float getTemp() {
        return temp;
    }

    public void setTemp(float temp) {
        this.temp = hasForfait() ? temp : 0;
    }

    @Override
    public void affiche(Screen s) {
        if (hasForfait()) {
            s.show(this.detail() + " " + getTemp() + " secondes.");
        } else {
            s.show(this.detail() + " doit se procurer un forfait avant le début de l'épreuve");
        }
    }
}
