package org.tp.exos.nombre;

import org.tp.Screen;
import org.tp.Showable;

public class Nombre implements Showable {

    private int n;

    public Nombre () {
        n = (int)(Math.random() * 101);
    }

    @Override
    public void affiche(Screen s) {
        s.show("Le nombre aleatoire est: " + getNombre());
    }

    public int getNombre() {
        return n;
    }
}
