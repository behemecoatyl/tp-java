package org.tp.exos.notes;

public class EnsembleNotes {
    private int[] notes;
    private int nb_notes;

    public EnsembleNotes() {
        notes = new int[]{0,0,0,0,0,0,0,0,0,0};
        nb_notes = 0;
    }

    public void ajout(int note) {
        notes[nb_notes] = note;
        nb_notes++;
    }

    public void enlever(int i) {
        for (i = i - 1; i < nb_notes; i++) {
            notes[i] = i >= notes.length ? 0 : notes[i+1];
        }
        nb_notes--;
    }

    public float moyenne() {
        float moyenne = 0;
        for (int note : notes) {
            moyenne += note;
        }
        return moyenne / nb_notes;
    }

    public int plusPetite() {
        int min = 0;
        for (int i = 0; i < nb_notes; i++) {
            if (notes[i] < min) {
                min = notes[i];
            }
        }
        return min;
    }

    public int plusGrande() {
        int max = 0;
        for (int i = 0; i < nb_notes; i++) {
            if (notes[i] > max) {
                max = notes[i];
            }
        }
        return max;
    }

    public int lireNote(int i) {
        return notes[i - 1];
    }

    public int nombre() {
        return nb_notes;
    }

    public String liste() {
        String s = "";
        for (int i = 0; i < nb_notes; i++) {
            s += notes[i] + " ";
        }
        return s.trim();
    }
}
