package org.tp.exos.notes;

import org.tp.Screen;
import org.tp.Showable;

public class Etudiant implements Showable {
    private String nom;
    private String prenom;
    private EnsembleNotes notes;

    public Etudiant(String n, String p) {
        nouvelleIdentite(n, p);
        notes = new EnsembleNotes();
    }

    public void nouvelleIdentite(String n, String p) {
        nom = n;
        prenom = p;
    }

    public void ajouterNote(int note) {
        notes.ajout(note);
    }

    public void enleverNote(int i) {
        notes.enlever(i);
    }

    @Override
    public void affiche (Screen s) {
        s.show(nom + " " + prenom);
        s.show("moyenne: " + notes.moyenne());
        s.show("moyenne des " + notes.nombre() + " notes: " + notes.liste());
    }
}
