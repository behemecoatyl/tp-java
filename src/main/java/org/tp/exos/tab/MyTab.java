package org.tp.exos.tab;

public class MyTab extends Tableau<Integer> {
    public static final int min = 0;
    public static final int max = 100;

    public MyTab(int n) {
        initialise(n);
    }

    public float moyenne() {
        float acc = 0;
        for (int i : getTableau()) {
            acc += i;
        }
        return acc / length();
    }

    public int min() {
        int min = MyTab.max;
        for (int i : getTableau()) {
            if (i < min) {
                min = i;
            }
        }
        return min;
    }

    public int max() {
        int max = MyTab.min;
        for (int i : getTableau()) {
            if (i > max) {
                max = i;
            }
        }
        return max;
    }

    public void rightShift() {
        int last = get(length() - 1);
        for (int i = length() - 1; i >= 0; i--) {
            set(i, (i == 0 ? last : get(i-1)));
        }
    }

    public int somme() {
        int acc = 0;
        for (int i : getTableau()) {
            acc += i;
        }
        return acc;
    }

    public void increment(int n) {
        for (int i = 0; i < length(); i++) {
            set(i, get(i) + n);
        }
    }

    public void tri() {
        // simple insertion sort implementation

        int cpt;
        int element;

        for (int i = 1; i < length() ; i++) {
            element = get(i);
            cpt = i - 1;
            while (cpt >= 0 && get(cpt) > element) {
                set(cpt + 1, get(cpt));
                cpt--;
            }
            set(cpt + 1, element);
        }
    }

    @Override
    protected void initialise (int n) {
        a = new Integer[n];
        for (int i = 0 ; i < a.length; i++) {
            a[i] = (int)(Math.random() * (max - min + 1) + min);
        }
    }
}
