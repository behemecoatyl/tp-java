package org.tp.exos.tab;

import org.tp.Screen;
import org.tp.Showable;


public abstract class Tableau<T> implements Showable {
    protected T[] a;

    public T[] getTableau() {
        return a;
    }

    public T get(int i) {
        return validIndex(i) ? a[i] : null;
    }

    public void set(int i, T val) {
        if (validIndex(i)) {
            a[i] = val;
        }
    }

    protected boolean validIndex(int i) {
        return i < length() && i >= 0;
    }

    protected abstract void initialise(int n);

    public int length() {
        return a.length;
    }

    @Override
    public void affiche (Screen s) {
        StringBuilder str = new StringBuilder("Le tableu contient: ");
        for (T i : getTableau()) {
            str.append(i).append(" ");
        }
        s.show(str.toString().trim());
    }
}
