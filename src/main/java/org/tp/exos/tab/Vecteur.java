package org.tp.exos.tab;


import org.tp.Screen;

public class Vecteur extends Tableau<Float> {
    public Vecteur(int n) {
        initialise(n);
    }

    public Vecteur(int n, float x) {
        initialise(n, x);
    }

    public Vecteur(Vecteur vec) {
        initialise(vec.length());
        for (int i = 0; i < vec.length(); i++) {
            set(i, vec.get(i));
        }
    }

    public float prod(Vecteur other) {
        if (length() != other.length()) {
            return 0;
        }
        float p = 0;
        for (int i = 0; i < length(); i++) {
            p += get(i) * other.get(i);
        }
        return p;
    }

    public Vecteur somme(Vecteur other) {
        if (length() != other.length()) {
            return null;
        }
        Vecteur v = new Vecteur(length());
        for (int i = 0; i < length(); i++) {
            v.set(i, get(i) + other.get(i));
        }
        return v;
    }

    @Override
    protected void initialise (int n) {
        a = new Float[n];
        for (int i = 0 ; i < a.length; i++) {
            a[i] = (float)(Math.random() * (MyTab.max - MyTab.min + 1) + MyTab.min);
        }
    }

    protected void initialise (int n, float x) {
        a = new Float[n];
        for (int i = 0 ; i < a.length; i++) {
            a[i] = x;
        }
    }

    public String detail() {
        StringBuilder str = new StringBuilder("(");
        for (Float i : getTableau()) {
            str.append(i).append(" ");
        }
        return str.toString().trim() + ")";
    }

    public void affiche(Screen s) {
        s.show("vec : " + detail());
    }
}
