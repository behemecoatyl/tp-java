package org.tp.exos.caisse;

import org.junit.Test;
import org.junit.Before;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class CaisseTest {
    private float se = 256f;
    private float sc = 587.3f;
    private Caisse c;

    @Before
    public void setUp() throws Exception {
        c = new Caisse(se, sc);
    }

    @Test
    public void testMontant() throws Exception {
        assertThat(c.montant(), equalTo(se + sc));
    }

    @Test
    public void testEncaisserEspece() throws Exception {
        float e = 56.54f;
        c.encaisserEspece(e);
        assertThat(c.getSommeEspeces(), equalTo(se + e));
    }

    @Test
    public void testEncaisserCheque() throws Exception {
        float e = 56.54f;
        c.encaisserCheque(e);
        assertThat(c.getSommeCheques(), equalTo(sc + e));
    }

    @Test
    public void testEncaisserEspeceRendu() throws Exception {
        float don = 15.3f
            , du = 3.12f;
        c.encaisserEspeceRendu(du, don);
        assertThat(c.getSommeEspeces(), equalTo(se + don - du));
    }
}

