package org.tp.exos.compte;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class CompteTest {
    private String name = "jean michel";
    private float solde = 150.65f;
    private Compte c;

    @Before
    public void setUp () throws Exception {
        c = new Compte(name, solde);
    }

    @Test
    public void testGetTitulaire () throws Exception {
        assertThat(c.getTitulaire(), equalTo(name));
    }

    @Test
    public void testSetTitulaire () throws Exception {
        c.setTitulaire("johnny");
        assertThat(c.getTitulaire(), equalTo("johnny"));
    }

    @Test
    public void testGetSolde () throws Exception {
        assertThat(c.getSolde(), equalTo(solde));
    }

    @Test
    public void testSetSolde () throws Exception {
        c.setSolde(250);
        assertThat(c.getSolde(), equalTo(250f));
    }

    @Test
    public void testDebiter () throws Exception {
        int minus = 358;
        c.debiter(minus);
        assertThat(c.getSolde(), equalTo(solde - minus));
    }

    @Test
    public void testCrediter () throws Exception {
        int plus = 358;
        c.crediter(plus);
        assertThat(c.getSolde(), equalTo(solde + plus));
    }
}
