package org.tp.exos.ecole;

import org.junit.Test;
import org.junit.Before;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class EcoleTest {
    private String name = "greta";
    private int nb_students = 150;
    private Ecole e;

    @Before
    public void setUp() throws Exception {
        e = new Ecole(name, nb_students);
    }

    @Test
    public void testGetNom () throws Exception {
        assertThat(e.getNom(), equalTo(name));
    }

    @Test
    public void testSetNom () throws Exception {
        String n = "flaubert";
        e.setNom(n);
        assertThat(e.getNom(), equalTo(n));
    }

    @Test
    public void testGetNombreEtudiant () throws Exception {
        assertThat(e.getNombreEtudiant(), equalTo(nb_students));
    }

    @Test
    public void testSetNombreEtudiant () throws Exception {
        int new_nb = -50;
        e.setNombreEtudiant(new_nb);
        assertThat(e.getNombreEtudiant(), equalTo(0));
        new_nb = Math.abs(new_nb);
        e.setNombreEtudiant(new_nb);
        assertThat(e.getNombreEtudiant(), equalTo(new_nb));
    }

    @Test
    public void testEnleverEtudiant () throws Exception {
        int minus = 80;
        e.enleverEtudiant(80);
        assertThat(e.getNombreEtudiant(), equalTo(nb_students - minus));
    }

    @Test
    public void testCrediterEtudiant () throws Exception {
        int plus = 80;
        e.crediterEtudiant(80);
        assertThat(e.getNombreEtudiant(), equalTo(nb_students + plus));
    }
}
