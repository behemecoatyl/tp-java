package org.tp.exos.ecoleinge;

import org.junit.Test;
import org.junit.Before;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class EcoleIngeTest {
    private int prepa = 100;
    private int inge = 150;
    private EcoleInge e;

    @Before
    public void setUp() throws Exception {
        e = new EcoleInge(prepa, inge);
    }
    @Test
    public void testInitialiser () throws Exception {
        e.initialiser();
        assertThat(e.nombreEtudiants(), equalTo(0));
    }

    @Test
    public void testGetEtudiantInge () throws Exception {
        assertThat(e.getEtudiantInge(), equalTo(inge));
    }

    @Test
    public void testSetEtudiantInge () throws Exception {
        e.setEtudiantInge(180);
        assertThat(e.getEtudiantInge(), equalTo(180));
    }

    @Test
    public void testGetEtudiantPrepa () throws Exception {
        assertThat(e.getEtudiantPrepa(), equalTo(prepa));
    }

    @Test
    public void testSetEtudiantPrepa () throws Exception {
        e.setEtudiantPrepa(180);
        assertThat(e.getEtudiantPrepa(), equalTo(180));
    }

    @Test
    public void testNombreEtudiants () throws Exception {
        assertThat(e.nombreEtudiants(), equalTo(prepa + inge));
    }

    @Test
    public void testAjouterEtudiantInge () throws Exception {
        e.ajouterEtudiantInge(150);
        assertThat(e.getEtudiantInge(), equalTo(inge + 150));
    }

    @Test
    public void testEnleverEtudiantInge () throws Exception {
        int minus;
        minus = inge * 2;
        e.enleverEtudiantInge(minus);
        assertThat(e.getEtudiantInge(), equalTo(0));
        setUp();
        minus = inge / 2;
        e.enleverEtudiantInge(minus);
        assertThat(e.getEtudiantInge(), equalTo(inge - minus));
    }

    @Test
    public void testAjouterEtudiantPrepa () throws Exception {
        e.ajouterEtudiantPrepa(150);
        assertThat(e.getEtudiantPrepa(), equalTo(prepa + 150));
    }

    @Test
    public void testEnleverEtudiantPrepa () throws Exception {
        int minus;
        minus = prepa * 2;
        e.enleverEtudiantPrepa(minus);
        assertThat(e.getEtudiantPrepa(), equalTo(0));
        setUp();
        minus = prepa / 2;
        e.enleverEtudiantPrepa(minus);
        assertThat(e.getEtudiantPrepa(), equalTo(prepa - minus));
    }
}
